import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { BehaviorSubject } from 'rxjs';

export interface LinkProfile {
  profile: string;
  title?: string;
  description?: string;
  rel?: string; // canonical or alternate
  url: string;
  type?: string;
}
@Injectable()
export class NgxProfilesService {

  private static AcceptProfile = 'Accept-Profile';

  private _linkProfile: LinkProfile = null;

  public profile: BehaviorSubject<LinkProfile> = new BehaviorSubject(null);

  constructor(private context: AngularDataContext, private http: HttpClient) {

   }

   /**
    * Parses Link header and returns an array of link profiles
    * Read more at https://www.w3.org/TR/dx-prof-conneg/#listprofiles
    * @param header
    * @returns Array<LinkProfile>
    */
   static parseLinkHeader(header: string): Array<LinkProfile> {
     if (header == null) {
       return [];
     }
     // split header items by ','
     const items = header.split(',');
     const results = items.map((item) => {
       // split properties by '='
      const properties = item.split(';');
      const result = {
        url: properties[0].replace(/^<+|>+$/g, '')
      };
      properties.splice(0, 1);
      return properties.reduce((obj, str) => {
        const value = str.split('=');
        Object.defineProperty(obj, value[0], {
          configurable: true,
           enumerable: true,
           writable: true,
           value: decodeURIComponent(value[1].replace(/^"+|"+$/g, ''))
        });
        return obj;
      }, result);
     });
     return results as Array<LinkProfile>;
   }

   /**
    * Checks if target api server supports link profiles
    * @returns
    */
   supported(): Promise<boolean> {
     return this.context.model('diagnostics/services').asQueryable().getItems().then((services) => {
      const hasProfileService = services.find((item: {serviceType: string, strategyType: string}) => {
        return item.serviceType === 'ProfileService';
      });
      return hasProfileService != null;
     });
   }

   /**
    * Gets student profiles of the current user
    * @returns Promise<Array<LinkProfile>>
    */
   async getStudentProfiles(): Promise<Array<LinkProfile>> {
     const serviceUrl = this.context.getService().resolve('students/me');
     const headers = this.context.getService().getHeaders();
     const response = await this.http.head(serviceUrl, {
       observe: 'response',
       headers: headers,
       withCredentials: true
     }).toPromise();
     // get link header
     const linkHeader = response.headers.get('Link');
     // parse link headers
     return await Promise.resolve(NgxProfilesService.parseLinkHeader(linkHeader));
   }
   /**
    * Sets the current student profile
    * @param linkProfile
    * @returns void
    */
   setStudentProfile(linkProfile: LinkProfile): this {
     this._linkProfile = linkProfile;
     // get user
     const currentUser = JSON.parse(sessionStorage.getItem('currentUser'));
     // validate params and throw error if profile is not null but current user is null
     if (linkProfile != null && linkProfile.profile != null && currentUser == null) {
       throw new Error('You cannot set user profile when current user is inaccessible.');
     }
     if (currentUser != null) {
       // assign user profile
      Object.assign(currentUser, {
        profile: linkProfile && linkProfile.profile ? linkProfile.profile : null
      });
      sessionStorage.clear();
      // set session storage item
      sessionStorage.setItem('currentUser', JSON.stringify(currentUser));
     }
     // raise event to let application reload user data
     this.profile.next(this._linkProfile);
     // and return
     return this;
   }

}
