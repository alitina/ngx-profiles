import { APP_BASE_HREF } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, TestRequest } from '@angular/common/http/testing';
import { inject, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { AngularDataContext, MostModule } from '@themost/angular';
import { ConfigurationService, SharedModule } from '@universis/common';
import { ApiTestingController, ApiTestingModule, TestingConfigurationService } from '@universis/common/testing';

import { NgxProfilesService } from './ngx-profiles.service';

describe('NgxProfilesService', () => {

  let mockApi: ApiTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        TranslateModule.forRoot(),
        MostModule.forRoot({
          base: '/api/',
          options: {
            useMediaTypeExtensions: false
          }
        }),
        SharedModule,
        ApiTestingModule.forRoot()
      ],
      providers: [
        {
          provide: ConfigurationService,
          useClass: TestingConfigurationService
        },
        {
          provide: APP_BASE_HREF,
          useValue: '/'
        }
      ],
      declarations: [
      ]
    });
    mockApi = TestBed.get(ApiTestingController);
  });

  it('should be created', inject([AngularDataContext, HttpClient], (context: AngularDataContext, http: HttpClient) => {
    const service = new NgxProfilesService(context, http);
    expect(service).toBeTruthy();
  }));
  it('should parse link header', inject([AngularDataContext, HttpClient], (context: AngularDataContext, http: HttpClient) => {
    const linkHeader = '<http://localhost:5001/api/students/me>;rel="canonical";type="application/json";' +
      'profile="urn:universis:student:profile:33456111";title="Undergraduate study program A",' +
      '<http://localhost:5001/api/students/me>;rel="alternate";type="application/json";' +
      'profile="urn:universis:student:profile:22445523";title="Postgraduate study program B"';
    const profiles = NgxProfilesService.parseLinkHeader(linkHeader);
    expect(profiles).toBeTruthy();
    expect(profiles.length).toBe(2);
    let linkProfile = profiles[0];
    expect(linkProfile.url).toBe('http://localhost:5001/api/students/me');
    expect(linkProfile.profile).toBe('urn:universis:student:profile:33456111');
    expect(linkProfile.rel).toBe('canonical');
    expect(linkProfile.title).toBe('Undergraduate study program A');
    linkProfile = profiles[1];
    expect(linkProfile.profile).toBe('urn:universis:student:profile:22445523');
  }));

  it('should get profiles', inject([AngularDataContext, HttpClient], async (context: AngularDataContext, http: HttpClient) => {
    const service = new NgxProfilesService(context, http);
    mockApi.match({
      url: '/api/students/me',
      method: 'HEAD'
    }
    ).map((request: TestRequest) => {
      request.flush(null, {
        headers: {
          Link: '<http://localhost:5001/api/students/me>;rel="canonical";type="application/json";' +
            'profile="urn:universis:student:profile:33456111";title="Undergraduate study program A",' +
            '<http://localhost:5001/api/students/me>;rel="alternate";type="application/json";' +
            'profile="urn:universis:student:profile:22445523";title="Postgraduate study program B"'
        }
      });
    });
    const results = await service.getStudentProfiles();
    expect(results).toBeTruthy();
    const linkProfile = results[0];
    expect(linkProfile.url).toBe('http://localhost:5001/api/students/me');
    expect(linkProfile.profile).toBe('urn:universis:student:profile:33456111');
    expect(linkProfile.rel).toBe('canonical');
    expect(linkProfile.title).toBe('Undergraduate study program A');
  }));

  it('should set profile', inject([AngularDataContext, HttpClient], async (context: AngularDataContext, http: HttpClient) => {
    const service = new NgxProfilesService(context, http);
    mockApi.match({
      url: '/api/students/me',
      method: 'HEAD'
    }
    ).map((request: TestRequest) => {
      request.flush(null, {
        headers: {
          Link: '<http://localhost:5001/api/students/me>;rel="canonical";type="application/json";' +
            'profile="urn:universis:student:profile:33456111";title="Undergraduate study program A",' +
            '<http://localhost:5001/api/students/me>;rel="alternate";type="application/json";' +
            'profile="urn:universis:student:profile:22445523";title="Postgraduate study program B"'
        }
      });
    });
    const results = await service.getStudentProfiles();
    service.profile.subscribe((activeProfile) => {
      if (activeProfile == null) {
        return;
      }
      expect(activeProfile).toBeTruthy();
      expect(activeProfile.profile).toBe('urn:universis:student:profile:33456111');
    });
    service.setStudentProfile(results[0]);
  }));

  it('should validate support of link profiles', inject([AngularDataContext, HttpClient],
    async (context: AngularDataContext, http: HttpClient) => {
    const service = new NgxProfilesService(context, http);
    mockApi.match({
      url: '/api/diagnostics/services/',
      method: 'GET'
    }).map((request: TestRequest) => {
      request.flush([
        {
          serviceType: 'ProfileService'
        }
      ]);
    });
    const value = await service.supported();
    expect(value).toBeTruthy();
  }));


});
